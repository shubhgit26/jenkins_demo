#! /bin/bash

operation=$1

function createBranches()
{
 branchname=$1
 git branch "{branchname}"
 git branch 
 echo "Successfuly Created Branch"
}
 
function listBranches()
{ 
 echo "Listing All Branches"
 git branch -r
 echo "===================="
 echo "Listing All Local Branches"
 git branch
}
 
function deleteBranch()
{
barnchname=$1
git branch --delete "{branchname}"
echo "Succussfully Deleted Branch"
git branch
}

function mergeBranch()
{
    basebranch=$1
    mergebranch=$2
    git branch > /tmp/gitbranches.txt
    tmp=`awk '/* /{print}' /tmp/gitbranches.txt`
        if [ "${tmp}" = "* ${basebranch}" ]; then
            git merge "${mergebranch}"
        else
            echo "Enter Valid Branch Name."
        fi
}

function rebaseBranch()
{
    currentbranch=$1
    rebasebranch=$2
    git branch > /tmp/gitbranches.txt
    tmp=`awk '/* /{print}' /tmp/gitbranches.txt`
        if [ "${tmp}" = "* ${currentbranch}" ]; then
            git rebase "${rebasebranch}"
        else
            echo "Enter Valid Current Branch Name."
        fi
}
case $operation in
    listBranches)
        listBranches
        ;;
    createBranches)
        createBranches "$2"
        ;;
    deleteBranch)
        deleteBranch "$2"
        ;;
    mergeBranch)
        mergeBranch "$2" "$3"
        ;;
    rebaseBranch)
        rebaseBranch "$2" "$3"
        ;;
   
    *)
        echo "Enter Valid Opetation from -- listBranches, createBranch, deleteBranch, mergeBranch, rebaseBranch"
        ;;
esac
